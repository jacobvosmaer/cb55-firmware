#ifndef MIDI_H
#define MIDI_H

#include <stdint.h>

uint8_t midi_read(uint8_t *key, uint8_t *velocity);
void midi_set_channel(uint8_t channel);

uint8_t uart_read(uint8_t *data);

void sysex_begin(void);
void sysex_end(void);
void sysex_recv(uint8_t x);

#endif
