#include "midi.h"
#include <avr/eeprom.h>
#include <avr/io.h>
#include <string.h>

enum _triggers {
	TRIG_BD,     // bass drum
	TRIG_SD,     // snare drum
	TRIG_RM,     // rimshot
	TRIG_HH,     // hihat
	TRIG_ACCENT, // accent
	NUM_TRIGS
};

struct {
	const uint8_t mask;
	const uint8_t key;
	uint8_t counter;
} triggers[NUM_TRIGS] = {
    {.mask = _BV(PORTD6), .key = 36}, // TRIG_BD C3
    {.mask = _BV(PORTD3), .key = 38}, // TRIG_SD D3
    {.mask = _BV(PORTD5), .key = 34}, // TRIG_RM A#2
    {.mask = _BV(PORTD4), .key = 42}, // TRIG_HH F#3
    {.mask = _BV(PORTD7),
     .key = 255}, // TRIG_ACCENT (key 255 prevents midi trigger)
};

enum { LED_MASK = _BV(PORTB5),
       // We run Timer0 at F_CPU/1024. With PULSE at 160 ticks, it takes
       // 160*1024/16000000 =~ 10ms.
       PULSE = 160,
};

static uint8_t EEMEM stored_midi_channel;

int main(void) {
	for (uint8_t i = 0; i < NUM_TRIGS; i++) {
		DDRD |= triggers[i].mask;
	}

	// LED out
	DDRB |= LED_MASK;

	// Run Timer0 at F_CPU/1024 Hz
	TCCR0B = _BV(CS02) | _BV(CS00);

	// Set UART to receive at 31.25 kbaud
	UBRR0H = 0;
	UBRR0L = 31;
	UCSR0B = _BV(RXEN0);

	midi_set_channel(eeprom_read_byte(&stored_midi_channel));

	uint8_t prev_time = TCNT0;

	for (;;) {
		uint8_t key, velocity;
		if (midi_read(&key, &velocity) && velocity) {
			for (uint8_t i = 0; i < NUM_TRIGS; i++) {
				if (triggers[i].key == key) {
					triggers[i].counter = PULSE;
					break;
				}
			}

			if (velocity > 100) {
				triggers[TRIG_ACCENT].counter = PULSE;
			}
		}

		uint8_t cur_time = TCNT0;
		uint8_t delta = cur_time - prev_time;
		prev_time = cur_time;
		uint8_t any_trigger_active = 0;

		for (uint8_t i = 0; i < NUM_TRIGS; i++) {
			if (triggers[i].counter > delta) {
				triggers[i].counter -= delta;
				PORTD |= triggers[i].mask;
				any_trigger_active = 1;
			} else {
				triggers[i].counter = 0;
				PORTD &= ~triggers[i].mask;
			}
		}

		if (any_trigger_active) {
			PORTB |= LED_MASK;
		} else {
			PORTB &= ~LED_MASK;
		}
	}

	return 0;
}

uint8_t uart_read(uint8_t *data) {
	uint8_t uart_status = UCSR0A;
	if (!(uart_status & _BV(RXC0))) {
		return 0;
	}

	*data = UDR0;
	return !(uart_status & (_BV(FE0) | _BV(DOR0) | _BV(UPE0)));
}

struct {
	uint8_t buf[3]; // 0x60, 0x55, 0x0n
	uint8_t len;
	uint8_t overflow;
} sysex = {
    .buf = {0},
};

void sysex_begin(void) {
	memset(sysex.buf, 0, sizeof(sysex.buf));
	sysex.len = 0;
	sysex.overflow = 0;
}

void sysex_end(void) {
	if (sysex.overflow || (sysex.len != sizeof(sysex.buf))) {
		return;
	}

	if (memcmp(sysex.buf, "\x60\x55", 2)) {
		return;
	}

	uint8_t channel = sysex.buf[2];
	eeprom_update_byte(&stored_midi_channel, channel);
	midi_set_channel(channel);
}

void sysex_recv(uint8_t x) {
	if (sysex.len >= sizeof(sysex.buf)) {
		sysex.overflow = 1;
		return;
	}

	sysex.buf[sysex.len] = x;
	sysex.len++;
}
