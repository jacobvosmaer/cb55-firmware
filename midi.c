#include "midi.h"

enum { MIDI_RESET,
       MIDI_EXPECT_KEY,
       MIDI_EXPECT_VELOCITY,
       MIDI_YIELD_NOTE_ON,
       MIDI_EXPECT_RLE,
       MIDI_EXPECT_SYSEX,
};

static struct {
	uint8_t key;
	uint8_t velocity;
	uint8_t state;
	uint8_t note_on_byte;
} midi = {0};

void midi_set_channel(uint8_t channel) {
	channel &= 0xf;
	midi.note_on_byte = 0x90 | channel;
	midi.state = MIDI_RESET;
}

uint8_t midi_is_data(uint8_t x) { return !(x & 0x80); }

uint8_t midi_is_realtime(uint8_t x) { return (x & 0xf8) == 0xf8; }

enum { SYSEX_BEGIN = 0xf0, SYSEX_END = 0xf7 };

void midi_parse(uint8_t b) {
	if (midi.state == MIDI_YIELD_NOTE_ON) {
		midi.state = MIDI_EXPECT_RLE;
	}

	if (midi_is_realtime(b)) {
		return;
	}

switch_state:
	switch (midi.state) {
	case MIDI_RESET:
		if (b == midi.note_on_byte) {
			midi.state = MIDI_EXPECT_KEY;
		} else if (b == SYSEX_BEGIN) {
			sysex_begin();
			midi.state = MIDI_EXPECT_SYSEX;
		}

		break;
	case MIDI_EXPECT_KEY:
		if (midi_is_data(b)) {
			midi.key = b;
			midi.state = MIDI_EXPECT_VELOCITY;
		} else {
			midi.state = MIDI_RESET;
		}

		break;
	case MIDI_EXPECT_VELOCITY:
		if (midi_is_data(b)) {
			midi.velocity = b;
			midi.state = MIDI_YIELD_NOTE_ON;
		} else {
			midi.state = MIDI_RESET;
		}

		break;
	case MIDI_EXPECT_RLE:
		if (midi_is_data(b)) {
			midi.key = b;
			midi.state = MIDI_EXPECT_VELOCITY;
		} else {
			// The caller is not using run length encoding. Fall
			// back to normal parsing.
			midi.state = MIDI_RESET;
			goto switch_state;
		}

		break;
	case MIDI_EXPECT_SYSEX:
		if (midi_is_data(b)) {
			sysex_recv(b);
		} else if (b == SYSEX_END) {
			sysex_end();
			midi.state = MIDI_RESET;
		} else {
			midi.state = MIDI_RESET;
		}

		break;
	}
}

uint8_t midi_read(uint8_t *key, uint8_t *velocity) {
	uint8_t data;
	if (!uart_read(&data)) {
		return 0;
	}

	midi_parse(data);

	*key = midi.key;
	*velocity = midi.velocity;
	return midi.state == MIDI_YIELD_NOTE_ON;
}
