#include "midi.h"
#include <assert.h>
#include <stdint.h>
#include <stdio.h>

#define desc(s) printf("TEST: %s\n", s)

uint8_t next_byte = 0;

uint8_t uart_read(uint8_t *data) {
	*data = next_byte;
	return 1;
}

void test_init(void) {
	desc("test_init");
	midi_set_channel(0);
}

void test_rle(void) {
	desc("test_rle");
	uint8_t k, v;

	next_byte = 0x90;
	assert(!midi_read(&k, &v));
	next_byte = 1;
	assert(!midi_read(&k, &v));
	next_byte = 2;
	assert(midi_read(&k, &v));
	assert(k == 1);
	assert(v == 2);

	next_byte = 3;
	assert(!midi_read(&k, &v));
	next_byte = 4;
	assert(midi_read(&k, &v));
	assert(k == 3);
	assert(v == 4);

#define real_time_garbage                                                      \
	{                                                                      \
		next_byte = 0xf8;                                              \
		assert(!midi_read(&k, &v));                                    \
	}

	real_time_garbage;
	next_byte = 5;
	assert(!midi_read(&k, &v));
	real_time_garbage;
	next_byte = 6;
	assert(midi_read(&k, &v));
	assert(k == 5);
	assert(v == 6);

	next_byte = 7;
	assert(!midi_read(&k, &v));
	real_time_garbage;
	next_byte = 8;
	assert(midi_read(&k, &v));
	assert(k == 7);
	assert(v == 8);
}

int main(void) {
	test_init();
	test_rle();

	return 0;
}

void sysex_begin(void) {}
void sysex_end(void) {}
void sysex_recv(uint8_t x) {}
