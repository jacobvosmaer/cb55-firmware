# CB-55 firmware

Under development. The plan is to use an 'Arduino Pro Mini' from
Sparkfun as a MIDI-to-trigger converter for the CB-55. This requires
an opto-isolator circuit; I want to use a 6N137 for that.

The programmer I'm using is the 'FTDI Basic' from Sparkfun.
